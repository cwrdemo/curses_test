//
//  main.c
//  curses_test1
//
//  Created by Will Roethel on 12/20/15.
//  Copyright © 2015 cwroethel.net. All rights reserved.
//
//  The goal of the game is to catch the falling 'A' with the 'U'.
//  At a random location a 'A' will drop from the top of the screen.
//  The player controls the 'U' with the directional button.
//
//  The game screen is a typical terminal screen with 24 rows and 80 columns.
//    o row 0 is the top row. The last row is at postion nrows-1
//    o col 0 is the left most column; the last column is at position ncols-1


#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <ncurses.h>

// the main game window
WINDOW *wnd;

// store the number of rows and columns of the screen
int nrows, ncols;

// game control variables
int current_col; // current postion of the 'U'

// position of the falling 'A'
int a_count;      // number of falling 'A' currently in the game
int a_col, a_row; // row and column of the falling 'A'

// game score
int score;

// delay timer for nanosleep
struct timespec ts = {.tv_sec = 0, .tv_nsec = 100000000};


//
// draw the main screen
//
void init_screen() {
    
    // create the game window
    wnd = initscr();         // curses call to initialize window

    cbreak();                // curses call to set no waiting for Enter key
    noecho();                // curses call to set no echoing, i.e. don't print pressed key
    curs_set(0);             // hide the cursor
    nodelay(wnd, true);      // don't wait for keys pressed
    
    // get screen size
    getmaxyx(wnd, nrows, ncols); // curses call to find size of window
    clear();                     // curses call to clear screen, send cursor to position (0,0)
    refresh();                   // curses call to implement all changes since last refresh
}




//
// move_player
// check keyboard input and reset the postion of the player
//
int move_player() {
    
    // store the old column position
    int old_col = current_col;
    
    // get input from the keyboard
    int d = getch();    // curses call to input from keyboard
    
    switch (d) {
            
            // quit gatme
        case 'q':
            return 1;
            
            
            // left key
        // case KEY_LEFT:
        case 'k':
            // move one column to the left
            current_col--;
            
            // Rest the current postion to the far right if the player
            // moves off the screen on the left
            if (current_col < 0) {
                current_col = ncols;
            }
            break;
            
            
            // right key
        // case KEY_RIGHT:
        case 'l':
            // move one column to the right
            current_col++;
            
            // Reset position to the far left if the player moves off the
            // right side of the screen
            if (current_col > ncols-1) {
                current_col = 0;
            }
            break;
            
        default:
            break;
    }
    
    // draw the player at the new postion if the postion changed
    if (old_col != current_col) {
        mvprintw(nrows-1, old_col, "%c", ' ');
        mvprintw(nrows-1, current_col, "%c", 'U');
    }
    
    return 0;
}


//
// move the 'A'
//
int move_a() {
    
    // create a new 'A' if a_count is 0
    if (a_count == 0) {
        a_col = (int)((double)rand()/(double)RAND_MAX * ncols);
        a_row = 0;
        a_count++;
    }
    
    
    // calculate the new position of the 'A'
    a_row++;
    
    mvprintw(a_row-1, a_col, "%c", ' ');
    mvprintw(a_row, a_col, "%c", 'A');
    
    return 0;
}



void check_collision() {
    
    // first check if 'A' and 'U' are at the same spot
    if (a_row == nrows-1 && a_col == current_col) {
        score++; // increase the score
        a_count = 0;  // remove the 'A'
        mvprintw(a_row, a_col, "%c", 'O'); // replace with a 'O'
        
    }
    
    // check if 'A' fell off the screen
    if (a_row > nrows-1) {
        a_count = 0;
    }
}




//
// main loop.
//
void run() {

    // place the 'U' at the bottom of the screen.
    current_col = ncols/2;
    mvprintw(nrows-1, current_col, "%c", 'U');
    refresh();
    
    // initialize game variables and the random generator
    a_count = 0;  // set the 'A' count to 0
    srand((int)time(NULL));
    score = 0;    // set score to 0
    

    // this loop runs until break is called to end the game
    int move_count = 0;
    while (1) {
        move_count++;
        if (move_count == 3) move_count = 0;
        
        // call move player. If return value is 1 then
        // the game should be aborted.
        if (move_player() > 0) {
            break;
        }
        
        // move the 'A' only if move_count is 0
        if (move_count == 0) move_a();
        
        
        // check for collisions
        check_collision();
        

        refresh();
        nanosleep(&ts, NULL);
    }
    
}





//
// game summary
//
void game_over() {
    // clear screen
    
    // print game over
    mvprintw(nrows/2, ncols/2-5, "GAME OVER");

    // get input from the keyboard. Wait until any key is presses
    nodelay(wnd, false);
    getch();    // curses call to input from keyboard
}



//
// main program
//

int main() {
    
    // draw the main screen
    init_screen();
    
    // start the main game looo
    run();
    
    // print game summary
    game_over();
    
    // clean up
    endwin(); // curses call to restore the original window and leave

}


